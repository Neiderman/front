import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import BusinessIcon from '@material-ui/icons/Business';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { AppBar, FormControl, InputLabel, MenuItem, Select, Toolbar } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import axios from 'axios';

/* Componentes */
import Logout from '../../components/Logout';
import Copyright from '../../components/Copyright';
/* Componentes */

const storage = window.localStorage;

const useStyles = makeStyles((theme) => ({
    '@global': {
        ul: {
            margin: 0,
            padding: 0,
            listStyle: 'none',
        },
    },
    appBar: {
        borderBottom: `1px solid ${theme.palette.divider}`,
    },
    toolbar: {
        flexWrap: 'wrap',
    },
    toolbarTitle: {
        flexGrow: 1,
    },
    inputRoot: {
        color: 'inherit',
        background: '#dddadaa8',
        width: '100%',
        borderRadius: '15px',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),

        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: '20ch',
        },
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    }
}));

export default function SignUp() {
    const classes = useStyles();
    const [tipoDocumento, setTipoDocumento] = React.useState(2);

    function enviarForm(evt) {
        evt.preventDefault();

        axios({
            method: 'POST',
            url: `${process.env.REACT_APP_RUTA_CONSUMO_API}/v1/empresas/crear`,
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + storage.getItem("token")
            },
            data: {
                tipoDocumento: tipoDocumento,
                nroDocumento: document.getElementById('nroDocumento').value,
                razonSocial: document.getElementById('razonSocial').value
            }
        })
            .then(function (response) {
                if (response.data.status) {
                    alert('Empresa creada correctamente');
                    window.location.href = '/inicio'
                } else {
                    alert('Error: ' + response.data.message);
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    return (
        <React.Fragment>
            <CssBaseline />
            <AppBar position="static" color="default" elevation={0} className={classes.appBar}>
                <Toolbar className={classes.toolbar}>
                    <Typography variant="h6" color="inherit" noWrap className={classes.toolbarTitle}>
                        Documentación
                    </Typography>
                    <Logout />
                </Toolbar>
            </AppBar>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div className={classes.paper}>
                    <center>
                        <Avatar className={classes.avatar}>
                            <BusinessIcon />
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Nueva empresa
                        </Typography>
                        {/* <Alert severity="error">This is an error alert — check it out!</Alert> */}
                    </center>
                    <form className={classes.form} onSubmit={(evt) => { enviarForm(evt); }}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={12}>
                                <FormControl variant="outlined" className={classes.FormControl} style={{ minWidth: 395 }}>
                                    <InputLabel id="tipoDocumento_lb">Tipo de documento</InputLabel>
                                    <Select labelId="tipoDocumento_lb" id="tipoDocumento" label="Tipo de documento" defaultValue={"2"} required onChange={(evt) => { setTipoDocumento(evt.target.value); }}>
                                        <MenuItem value={"1"}>Cedula de ciudadania</MenuItem>
                                        <MenuItem value={"2"}>Nit</MenuItem>
                                        <MenuItem value={"3"}>Cedula de extranjeria</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} sm={12}>
                                <TextField variant="outlined" required fullWidth id="nroDocumento" name="nroDocumento" label="# Documento" inputProps={{ maxLength: 25 }} />
                            </Grid>
                            <Grid item xs={12} sm={12}>
                                <TextField variant="outlined" required fullWidth id="razonSocial" label="Razón social" inputProps={{ maxLength: 50 }} />
                            </Grid>
                        </Grid>

                        <br />
                        <Grid container>
                            <Grid item xs={5} sm={5}>
                                <Button type="button" fullWidth variant="contained" color="secondary" className={classes.submit} onClick={() => { window.location.href = '/'; }}>
                                    Cancelar
                                </Button>
                            </Grid>
                            <Grid item xs={2} sm={2}></Grid>
                            <Grid item xs={5} sm={5}>
                                <Button type="submit" fullWidth variant="contained" color="primary" className={classes.submit}>
                                    Crear
                                </Button>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Container>

            {/* Footer */}
            <Container maxWidth="md" component="footer" className={classes.footer}>
                <Box mt={5}>
                    <Copyright />
                </Box>
            </Container>
            {/* Footer */}

        </React.Fragment>
    );
}