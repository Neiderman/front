
import React, { useState } from "react";
import { Button, Grid, List, ListItem, ListItemIcon, ListItemText, TextField, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActionArea from '@material-ui/core/CardActionArea';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import YouTubeIcon from '@material-ui/icons/YouTube';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import OndemandVideoIcon from '@material-ui/icons/OndemandVideo';
import validator from 'validator';

const useStyles = makeStyles((theme) => ({
    '@global': {
        ul: {
            margin: 0,
            padding: 0,
            listStyle: 'none',
        },
    },
    appBar: {
        borderBottom: `1px solid ${theme.palette.divider}`,
    },
    toolbar: {
        flexWrap: 'wrap',
    },
    toolbarTitle: {
        flexGrow: 1,
    },
    link: {
        margin: theme.spacing(1, 1.5),
    },
    heroContent: {
        padding: theme.spacing(8, 0, 6),
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function CardsLoaders(props) {
    const classes = useStyles();
    const [open, setOpen] = useState(false);
    const [listaVideos, setVideos] = useState([])
    const [listaDocumentos, setDocumentos] = useState([])
    const [urlError, setUrlError] = useState(false)
    const [videoError, setVideoError] = useState(false)
    let url, alias;

    const handleClickOpen = () => {
        setOpen(true);
    };

    const onChangeUrl = (e) => {
        url = e.target.value;
    }

    const onChangeUrlName = (e) => {
        alias = e.target.value;
    }

    const guardarUrl = () => {
        if (validator.isURL(url)) {
            // setUrlError(false);
            setVideos([...listaVideos, { alias: alias, url: url }]);
            url = '';
            alias = '';
        } else {
            // setUrlError(true);
            alert(`URL ${url} no es valida.`);
            console.log(`URL ${url} no es valida.`);
        }
    };

    const handleClose = () => {
        setOpen(false);
    };

    const listadoVideos = listaVideos.map(e =>
        <ListItem style={{ cursor: 'pointer' }} onClick={() => { window.open(e.url, '_blank') }}>
            <ListItemIcon>
                <OndemandVideoIcon />
            </ListItemIcon>
            <ListItemText
                primary={e.alias}
                secondary={e.url}
            />
        </ListItem>
    );

    const listadoDocumentos = listaDocumentos.map(e =>
        <li>{e}</li>
    );

    return (
        <Grid container>
            <Grid container spacing={2} xs={6} style={{ marginTop: '10px' }}>
                <Grid item xs={6} style={{ border: '1px solid', borderRadius: 10 + 'px' }}>
                    <div className={classes.paper}>
                        <Typography component="h1" variant="h5">
                            Manuales y guías
                        </Typography>
                        <form className={classes.form} noValidate>
                            <Button type="button" fullWidth variant="contained" color="primary" className={classes.submit}>
                                Seleccionar
                            </Button>
                            <Button type="button" fullWidth variant="contained" color="primary" className={classes.submit}>
                                Cargar
                            </Button>
                        </form>
                    </div>
                </Grid>
                <Grid item xs={6} style={{ border: '1px solid', borderRadius: 10 + 'px' }}>
                    <div className={classes.paper}>
                        <Typography component="h1" variant="h5">
                            Videos
                        </Typography>
                        <form className={classes.form} noValidate>
                            <Button type="button" fullWidth variant="contained" color="primary" className={classes.submit}>
                                Seleccionar
                            </Button>
                            <Button type="button" fullWidth variant="contained" color="primary" className={classes.submit, 'btn_action'} variant="contained" onClick={handleClickOpen}>
                                Cargar
                            </Button>
                        </form>
                    </div>
                    <Dialog open={open} onClose={handleClose} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
                        <DialogTitle id="alert-dialog-title">{"Agregar video mediante link"}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                <center>
                                    <TextField id="alias_video" label="Nombre" onChange={onChangeUrlName} />
                                    <br />
                                    <TextField id="standard-basic" label="Link" onChange={onChangeUrl} />
                                </center>
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={guardarUrl,handleClose} color="secondary" autoFocus>
                                Guardar y finalizar
                            </Button>
                            <Button onClick={guardarUrl} color="primary">
                                Guardar y continuar
                            </Button>
                            <Button onClick={handleClose} color="secondary" autoFocus>
                                Cancelar
                            </Button>
                        </DialogActions>
                    </Dialog>
                </Grid>
            </Grid>
            <Grid container xs={6}>
                <Grid item xs={1}></Grid>
                <Grid container xs={11} style={{ border: '1px solid', borderRadius: 10 + 'px', padding: '5px', }}>
                    <Grid item xs={12}>
                        <Typography component="h1" variant="h5">
                            Manuales y guías
                        </Typography>
                        <hr />
                        {listaDocumentos.length < 1 ? (<center>No se han cargado Guías o Manuales</center>) : (<List dense>{listaDocumentos}</List>)}
                    </Grid>
                    <Grid item xs={12} style={{ borderTop: '2px solid' }}>
                        <Typography component="h1" variant="h5">
                            Videos
                        </Typography>
                        <hr />
                        {listadoVideos.length < 1 ? (<center>No se han cargado videos</center>) : (<List dense>{listadoVideos}</List>)}
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )

}

const classes = makeStyles((theme) => ({
    root: {
        maxWidth: 250,
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    media: {
        height: 140,
    },
}));




