import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid';
import ImageCard from '../Componentes/imageCard';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import { Container, Typography } from '@material-ui/core';

const storage = window.localStorage;
const urlPeticionListar = `${process.env.REACT_APP_RUTA_CONSUMO_API}/v1/equipos`;
const opcionesPeticion = {
    headers: {
        'Content-Type': 'application/json',
        'Accept': '*/*',
        'authorization': 'Bearer ' + storage.getItem('token')
    }
};

class ListadoEquipos extends Component {
    constructor() {
        super();
        this.state = { data: [] };
        this.tiempo = null;
    }

    componentDidMount() {
        fetch(urlPeticionListar, opcionesPeticion)
            .then(res => res.json())
            .then(json => this.setState({ data: json }))
            .catch(function (error) {
                console.log(error);
            });
    }

    ejecutarConsulta = (event) => {
        var filtro = event.target.value;
        if(event.target.value.length < 3) {
            filtro = "";
        }
        
        var self = this;
        if (self.tiempo != null) {
            clearInterval(self.tiempo);
        }
        
        self.tiempo = setInterval(function () {
            fetch(`${urlPeticionListar}?filtro=${encodeURIComponent(filtro)}`,opcionesPeticion)
                .then(res => res.json())
                .then(json => self.setState({ data: json }))
                .catch(function (error) {
                    self.state = { data: [] };
                });

            clearInterval(self.tiempo);
        }, 1000, "JavaScript");
    }

    redireccionVistaUnica(id) {
        window.location.href = `/documentacion/${id}`;
    }

    render() {
        const classes = this.props.classes;
        return (
            <React.Fragment>
                <Container maxWidth="sm" component="main" className={classes.heroContent}>
                    <div className={classes.search}>
                        <div className={classes.searchIcon}>
                            <SearchIcon />
                        </div>
                        <InputBase
                            title="Para buscar debes escribir como minimo 3 caracteres"
                            placeholder="Buscar …"
                            classes={{
                                root: classes.inputRoot,
                                input: classes.inputInput,
                            }}
                            inputProps={{ 'aria-label': 'search' }}
                            onChange={this.ejecutarConsulta}
                        />
                    </div>
                </Container>
                <Container maxWidth="md" component="main">
                    <Grid container spacing={5} alignItems="flex-end">
                        {this.state.data.equipos !== undefined && this.state.data.equipos.length > 0 ? (this.state.data.equipos.map((objeto, index) =>
                        <Grid item key={objeto._id} xs={12} sm={objeto.title === 'Enterprise' ? 12 : 6} md={4} onClick={() => this.redireccionVistaUnica(`${objeto._id}`)}>
                            <ImageCard Title={objeto.title} Image={`data:image/png;base64,${objeto.imagen}`} Nombre={objeto.nombre} Hardware={objeto.hardware_id} Referencia={objeto.referencia} />
                        </Grid>
                        )) : (
                            <Grid item xs={12} sm={12} md={12}>
                                <center><span>Sin equipos registrados</span></center>
                                {/* <ImageCard Title={tier.title} Image={tier.image} Description={tier.description} /> */}
                            </Grid>
                        )}
                    </Grid>
                </Container>
            </React.Fragment>




        );
    }
}

export default ListadoEquipos;