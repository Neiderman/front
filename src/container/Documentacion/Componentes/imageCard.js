import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
    },
});

function ImageCard({
    Title,
    Image,
    Hardware,
    Nombre,
    Referencia
}) {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardActionArea>
                <CardMedia
                    component="img"
                    alt="Contemplative Reptile"
                    height="140"
                    image={Image}
                    title="Contemplative Reptile"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        {Title}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="div">
                        <Typography component={'p'}><strong>Nombre</strong> <br /> {Nombre}</Typography>
                        <Typography component={'p'}><small>Referencia: {Referencia}</small> </Typography>
                        <Typography component={'p'}><small>Tipo: Brazo C Movil</small> </Typography>
                    </Typography>
                </CardContent>
            </CardActionArea>
            {/* <CardActions>
        <Button size="small" color="primary">
          Ver más
        </Button>
      </CardActions> */}
        </Card>
    );
}

ImageCard.propTypes = {
    Title: PropTypes.string,
    Image: PropTypes.string,
    Description: PropTypes.string
}

export default ImageCard;
