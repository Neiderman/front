import React, { useState, useEffect } from 'react';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import KeyboardReturnIcon from '@material-ui/icons/KeyboardReturn';
import FolderIcon from '@material-ui/icons/Folder';
import axios from 'axios';
import OndemandVideoIcon from '@material-ui/icons/OndemandVideo';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';

/* Componentes */
import Logout from './../../components/Logout'
import DesciptionCard from '../../components/DescriptionCard';
import CardsLoaders from './Componentes/CardsLoaders';
import { Button, Grid, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
/* Componentes */



const storage = window.localStorage;

const useStyles = makeStyles((theme) => ({
    '@global': {
        ul: {
            margin: 0,
            padding: 0,
            listStyle: 'none',
        },
    },
    appBar: {
        borderBottom: `1px solid ${theme.palette.divider}`,
    },
    toolbar: {
        flexWrap: 'wrap',
    },
    toolbarTitle: {
        flexGrow: 1,
    },
    link: {
        margin: theme.spacing(1, 1.5),
    },
    heroContent: {
        padding: theme.spacing(8, 0, 6),
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function Detalles(props) {
    const classes = useStyles();
    const [dense, setDense] = React.useState(false);
    const [secondary, setSecondary] = React.useState(false);
    const [equipos, setequipos] = useState([]);

    function requestToGetEquiposById() {
        var equipoId = props.match.params.id;

        axios({
            method: 'GET',
            url: `${process.env.REACT_APP_RUTA_CONSUMO_API}/v1/equipos/${equipoId}`,
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + storage.getItem("token")
            },
        })
            .then(function (response) {
                setequipos(response.data.equipos);
                console.log(response.data.equipos);
            })
            .catch(function (error) {
                console.log("error:", error);
            });
    }

    function generate(element) {
        return [0, 1, 2].map((value) =>
            React.cloneElement(element, {
                key: value,
            }),
        );
    }

    useEffect(() => {
        requestToGetEquiposById();
    }, []);

    return (
        <React.Fragment>
            <CssBaseline />
            <AppBar position="static" color="default" elevation={0} className={classes.appBar}>
                <Toolbar className={classes.toolbar}>
                    <Typography variant="h6" color="inherit" noWrap className={classes.toolbarTitle}>
                        Documentación
                    </Typography>
                    <Logout />
                </Toolbar>
            </AppBar>
            <Container maxWidth="md" component="main" style={{ padding: 5 + `px` }}>
                <Grid container spacing={1}>
                    <Grid item xs={3} style={{ padding: 20 + `px` }}>
                        <Button variant="contained" size="large" color="primary" onClick={() => { window.location.href = '/documentacion/' + props.match.params.id }} className={classes.margin}>
                            <KeyboardReturnIcon fontSize="large" />
                        </Button>
                    </Grid>
                    <Grid item xs={6}>
                        <Container maxWidth="sm" className={classes.heroContent} style={{ padding: 1 + 'px' }}>
                            <DesciptionCard
                                Image={equipos.imagen !== undefined ? `data:image/png;base64,${equipos.imagen}` : ""}
                                Name={equipos.nombre}
                                Code={equipos.referencia}
                                Type={equipos.tipo}
                            />
                        </Container>
                    </Grid>
                    <Grid item xs={3} style={{ padding: 20 + `px` }}>
                        <Button variant="contained" size="large" color="primary" style={{ marginLeft: 200 + 'px' }} className={classes.margin}>
                            Guardar
                        </Button>
                    </Grid>
                </Grid>
            </Container>

            <hr />

            <Container component="main" maxWidth="md">
                <CardsLoaders />
            </Container>

        </React.Fragment >
    );
}
