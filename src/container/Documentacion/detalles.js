import React, { useState, useEffect } from 'react';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { fade, makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

import SearchIcon from '@material-ui/icons/Search';
import KeyboardReturnIcon from '@material-ui/icons/KeyboardReturn';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';

import InputBase from '@material-ui/core/InputBase';
import Box from '@material-ui/core/Box';
import axios from 'axios';

/* Componentes */
import Copyright from './../../components/Copyright';
import Logout from './../../components/Logout'
import DesciptionCard from '../../components/DescriptionCard';
import { Button, Grid, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import ImageCard from './../../components/ImageCard/imageCard2';
/* Componentes */

const storage = window.localStorage;

const useStyles = makeStyles((theme) => ({
    '@global': {
        ul: {
            margin: 0,
            padding: 0,
            listStyle: 'none',
        },
    },
    appBar: {
        borderBottom: `1px solid ${theme.palette.divider}`,
    },
    toolbar: {
        flexWrap: 'wrap',
    },
    toolbarTitle: {
        flexGrow: 1,
    },
    link: {
        margin: theme.spacing(1, 1.5),
    },
    heroContent: {
        padding: theme.spacing(8, 0, 6),
    },
    cardHeader: {
        backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[700],
    },
    cardPricing: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'baseline',
        marginBottom: theme.spacing(2),
    },
    footer: {
        borderTop: `1px solid ${theme.palette.divider}`,
        marginTop: theme.spacing(8),
        paddingTop: theme.spacing(3),
        paddingBottom: theme.spacing(3),
        [theme.breakpoints.up('sm')]: {
            paddingTop: theme.spacing(6),
            paddingBottom: theme.spacing(6),
        },
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
        background: '#dddadaa8',
        width: '100%',
        borderRadius: '15px',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: '20ch',
        },
    },
    root: {
        flexGrow: 1,
    }
}));

export default function Detalles(props) {
    const classes = useStyles();
    const [equipos, setequipos] = useState([]);

    function requestToGetEquiposById() {
        var equipoId = props.match.params.id;

        axios({
            method: 'GET',
            url: `${process.env.REACT_APP_RUTA_CONSUMO_API}/v1/equipos/${equipoId}`,
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + storage.getItem("token")
            },
        })
            .then(function (response) {
                setequipos(response.data.equipos);
            })
            .catch(function (error) {
                console.log("error:", error);
            });
    }

    useEffect(() => {
        requestToGetEquiposById();
    }, []);

    return (
        <React.Fragment>
            <CssBaseline />
            <AppBar position="static" color="default" elevation={0} className={classes.appBar}>
                <Toolbar className={classes.toolbar}>
                    <Typography variant="h6" color="inherit" noWrap className={classes.toolbarTitle}>
                        Documentación
                    </Typography>
                    <Logout />
                </Toolbar>
            </AppBar>
            <Container maxWidth="md" component="main" className={classes.root} style={{ paddingTop: 20 + 'px' }}>
                <Container maxWidth="sm" component="main" className={classes.root} style={{ paddingTop: 20 + 'px' }}>
                    <div className={classes.search}>
                        <div className={classes.searchIcon}>
                            <SearchIcon />
                        </div>
                        <InputBase
                            placeholder="Buscar…"
                            classes={{
                                root: classes.inputRoot,
                                input: classes.inputInput,
                            }}
                            inputProps={{ 'aria-label': 'search' }}
                        />
                    </div>
                </Container>
            </Container>
            <Container maxWidth="md" component="main" className={classes.heroContent}>
                <Grid container spacing={1}>
                    <Grid item xs={3}>
                        <Button variant="contained" size="large" color="primary" onClick={() => { window.location.href = '/documentacion' }} className={classes.margin}>
                            <KeyboardReturnIcon fontSize="large" />
                        </Button>
                    </Grid>
                    <Grid item xs={6}>
                        <Container maxWidth="sm" className={classes.heroContent} style={{ padding: 1 + 'px' }}>
                            <DesciptionCard
                                Image={equipos.imagen !== undefined ? `data:image/png;base64,${equipos.imagen}` : ""}
                                Name={equipos.nombre}
                                Code={equipos.referencia}
                                Type={equipos.tipo}
                            />
                        </Container>
                    </Grid>
                    <Grid item xs={3}>
                        <Button variant="contained" size="large" color="primary" style={{ marginLeft: 200 + 'px' }} onClick={() => { window.location.href = '/documentacion/'+props.match.params.id+'/asignar' }} className={classes.margin}>
                            Editar documentación
                        </Button>
                    </Grid>
                </Grid>
            </Container>

            <hr></hr>

            {/* Videos */}
            <Container maxWidth="md" component="main" className={classes.heroContent}>
                <Typography component={'h5'}><strong>Videos</strong></Typography>

                <ImageCard></ImageCard>
            </Container>
            {/* Videos */}

            {/* Guias */}
            <Container maxWidth="md" component="main" className={classes.heroContent}>
                <Typography component={'h5'}><strong>Manuales y Guías</strong></Typography>

                <List component="nav" className={classes.root} aria-label="Manuales y Guías">
                    <ListItem>
                        <ListItemText inset primary={<strong>Nombre</strong>} />
                        <ListItemText primary={<strong>                                        Tipo</strong>} />
                        <ListItemText primary={<strong>Fecha creación</strong>} />
                    </ListItem>
                    <ListItem button>
                        <ListItemIcon>
                            <PictureAsPdfIcon />
                        </ListItemIcon>
                        <ListItemText primary="Como limpiar ecografo               " />
                        <ListItemText primary="Guía" />
                        <ListItemText primary="2020-07-15" />
                    </ListItem>
                    <ListItem button>
                        <ListItemIcon>
                            <PictureAsPdfIcon />
                        </ListItemIcon>
                        <ListItemText primary="Como limpiar grafeno                 " />
                        <ListItemText primary="Guía" />
                        <ListItemText primary="2020-06-12" />
                    </ListItem>
                    <ListItem button>
                        <ListItemIcon>
                            <PictureAsPdfIcon />
                        </ListItemIcon>
                        <ListItemText primary="Manual ECOGRAFO RMXAV2021" />
                        <ListItemText primary="Manual  " />
                        <ListItemText primary="2020-06-12" />
                    </ListItem>
                </List>
            </Container>
            {/* Guias */}

            {/* Footer */}
            <Container maxWidth="md" component="footer" className={classes.footer}>
                <Box mt={5}>
                    <Copyright />
                </Box>
            </Container>
            {/* End footer */}
        </React.Fragment>
    );
}
