import React from 'react';
import { Redirect } from 'react-router-dom';

/*
	* Import views
*/
import Documentacion from "./container/Documentacion";
import Empresas from "./container/Empresas";
import Personas from "./container/Personas";

/*
	* Constants
*/
const storage = window.localStorage;
const ROUTES_WITHOUT_LOGIN = ["/", "logout", "notfound"];
const PERMITTED_ROUTES = {
	documentacion: {
		component: Documentacion,
	},
	empresas: {
		component: Empresas,
	},
	personas: {
		component: Personas,
	}
	// , detalles: {
	// 	component: Detalles,
    
	// }
};

export default class Main extends React.Component {
	constructor(props) {
		super(props);

		console.log(this.props);
		this.state = {
			route: this.props.match.params.route,
			token: storage.getItem('token') === null || undefined ? false : true
		};
	}

	
	render(){
		var me = this.state;
		console.log(me);
		var PairComponent = (PERMITTED_ROUTES[me.route || 'dashboard'] || {}).component;

		if (!me.route && me.token) return <Redirect to={{pathname: "/dashboard",from: this.props.path}}/>

    const validRoute = (ROUTES_WITHOUT_LOGIN.indexOf(me.route) !== -1);
    if (validRoute) {
      return (
        <div>
          <PairComponent/>
        </div>
      );
    } else {
      return (
        !storage.getItem('token') || !PairComponent ? (
          window.location.pathname.indexOf("/") === -1?(window.location.pathname):(window.location = '/')): (
          <div>
            <PairComponent/>
          </div>
          )
      );
    }
	}
}
