import React, { Suspense, lazy } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

/* Validador de sesión activa */
const Main = lazy(() => import('./Main'));
/* Validador de sesión activa */

/* Auth */
const login = lazy(() => import('./container/auth/login'));
const registro = lazy(() => import('./container/auth/registro'));
/* Auth */

/* Inicio */
const Inicio = lazy(() => import('./container'));
/* Inicio */

/* Documentacion */
const Documentacion = lazy(() => import('./container/Documentacion'));
const Documentacion_Detalles = lazy(() => import('./container/Documentacion/detalles'));
const Documentacion_Detalles_Asignar = lazy(() => import('./container/Documentacion/asignar'));
/* Documentacion */

/* Empresas */
const Empresas = lazy(() => import('./container/Empresas'));
const Empresas_Crear = lazy(() => import('./container/Empresas/crear'));
/* Empresas */

/* Personas */
const Personas = lazy(() => import('./container/Personas'));
const Personas_Crear = lazy(() => import('./container/Personas/crear'));
/* Personas */

const App = () => (
  <Router>
    <Suspense fallback={<div>Cargando...</div>}>
      <Switch>
        <Route exact path="/" component={login}/>
        <Route exact path="/inicio" component={Inicio}/>
        <Route exact path="/personas" component={Personas}/>
        <Route exact path="/personas/crear" component={Personas_Crear}/>
        <Route exact path="/empresas" component={Empresas}/>
        <Route exact path="/empresas/crear" component={Empresas_Crear}/>
        <Route exact path="/documentacion" component={Documentacion}/>
        <Route exact path="/documentacion/:id" component={Documentacion_Detalles}/>
        <Route exact path="/documentacion/:id/asignar" component={Documentacion_Detalles_Asignar}/>
        <Route path="/:route" component={Main}/>
      </Switch>
    </Suspense>
  </Router>
);

export default App;
