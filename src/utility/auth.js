export function isBrowser() {
  return typeof window !== "undefined"
}

export function getUser() {
  return isBrowser() && window.localStorage.getItem("userInformation")
    ? JSON.parse(window.localStorage.getItem("userInformation"))
    : {}
}

export function setUser(user) {
  window.localStorage.setItem("userInformation", JSON.stringify(user))
}

export function isLoggedIn() {
  const user = getUser()

  return !!user.username
}

export function logout(callback) {
  setUser({})
  callback()
}
