import React from 'react';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  link: {
    margin: theme.spacing(1, 1.5),
  }
}));

export default function Logout() {
  const classes = useStyles();

  function clearStorage() {
    localStorage.clear();
    window.location = '/'
  };

  return (
    <Button color="primary" variant="outlined" className={classes.link} onClick={clearStorage}>
    Cerrar sesión
  </Button>
  );
}
