import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
    },
    cover: {
        width: 151,
    },
    controls: {
        display: 'flex',
        alignItems: 'center',
        paddingLeft: theme.spacing(1),
        paddingBottom: theme.spacing(1),
    },
    playIcon: {
        height: 38,
        width: 38,
    },
}));

function DesciptionCard({
    Image,
    Name,
    Code,
    Type
}) {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardMedia
                className={classes.cover}
                image={Image}
            />
            <div className={classes.details}>
                <CardContent className={classes.content}>
                    <Typography component="p" variant="h6">
                        <strong>Nombre:</strong> <br /> {Name}
                    </Typography>
                    <Typography variant="p" variant="h6">
                        <small>Referencia: {Code}</small>
                    </Typography>
                    <Typography variant="p" variant="h6">
                        <small>Tipo de equipo: {Type}</small>
                    </Typography>
                </CardContent>
            </div>
        </Card>
    );
}

DesciptionCard.propTypes = {
    Image: PropTypes.string,
    Name: PropTypes.string,
    Code: PropTypes.string,
    Type: PropTypes.string
}

export default DesciptionCard;
